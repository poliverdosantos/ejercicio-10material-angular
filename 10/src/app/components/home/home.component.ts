import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  //[] = [];
  valor: any;
  constructor(private servicioUsuario: UsuarioService) { }

  ngOnInit(): void {
    this.servicioUsuario.obtenerValor().subscribe({
      next: user => {
        console.log(user);
        this.valor = user['data'][4];
        console.log(user);
        
      },
      error: error => {
        console.log(error);
      },
    })
  }

}
